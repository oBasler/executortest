package one.adastra.executortest

import android.app.Activity
import android.os.Bundle
import android.util.Log
import com.aldebaran.qi.Future
import com.aldebaran.qi.sdk.QiContext
import com.aldebaran.qi.sdk.QiSDK
import com.aldebaran.qi.sdk.RobotLifecycleCallbacks
import com.aldebaran.qi.sdk.`object`.conversation.*
import com.aldebaran.qi.sdk.builder.*
import com.aldebaran.qi.sdk.design.activity.RobotActivity
import com.aldebaran.qi.sdk.design.activity.conversationstatus.SpeechBarDisplayStrategy
import org.jetbrains.anko.startActivity

import java.util.HashMap

class MainActivity : RobotActivity(), RobotLifecycleCallbacks {
    companion object {
        const val TAG = "MainActivity"
        fun start(context: Activity) = context.startActivity<MainActivity>()
    }

    private var chat: Chat? = null
    private var qiChatbot: QiChatbot? = null
    private var chatFuture: Future<Void>? = null

    override fun onRobotFocusGained(qiContext: QiContext) {
        Log.i(TAG, "Focus gained")

        val topic = TopicBuilder.with(qiContext)
            .withResource(R.raw.execute)
            .build()

        // Create a qiChatbot
        qiChatbot = QiChatbotBuilder
            .with(qiContext)
            .withTopic(topic)
            .build()
            .apply {

                addOnEndedListener {
                    stopChat()
                }
                val executors = HashMap<String, QiChatExecutor>()

                // Map the executor name from the topic to our qiChatbotExecutor
                executors["myExecutor"] = MyQiChatExecutor(qiContext)

                // Set the executors to the qiChatbot
                this.executors = executors
            }

        // Build chat with the chatbots
        chat = ChatBuilder
            .with(qiContext)
            .withChatbot(qiChatbot)
            .build()
            .apply {
                addOnStartedListener {
                    //Say proposal to user
                    val bookmark = topic.bookmarks["execute_proposal"]
                    qiChatbot?.goToBookmark(
                        bookmark,
                        AutonomousReactionImportance.HIGH,
                        AutonomousReactionValidity.IMMEDIATE
                    )
                }
            }

        chatFuture = chat
            ?.async()
            ?.run()
            ?.thenConsume { removeListeners() }
    }

    override fun onRobotFocusLost() {
        Log.i(TAG, "Focus lost.")
        stopChat()
    }

    override fun onRobotFocusRefused(reason: String?) {
        Log.i(TAG, "Focus refused")
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSpeechBarDisplayStrategy(SpeechBarDisplayStrategy.OVERLAY)
        QiSDK.register(this, this)
    }

    override fun onDestroy() {
        QiSDK.unregister(this, this)
        super.onDestroy()
    }

    private fun stopChat() {
        Log.i(TAG, "stopChat")
        chatFuture?.requestCancellation()
    }

    private fun removeListeners() {
        Log.i(TAG, "removeListeners")
        qiChatbot?.removeAllOnEndedListeners()
        chat?.removeAllOnStartedListeners()
    }

    internal inner class MyQiChatExecutor(qiContext: QiContext) : BaseQiChatExecutor(qiContext) {

        override fun runWith(params: List<String>) {
            // This is called when execute is reached in the topic
            Log.i(TAG, "QiChatExecutor executed")
        }

        override fun stop() {
            // This is called when chat is canceled or stopped.
            Log.i(TAG, "QiChatExecutor stopped")
        }
    }
}
